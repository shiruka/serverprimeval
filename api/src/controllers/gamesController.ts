import {Request, Response} from 'express';
import pool from '../../database';

class GamesController {

    public async list(req: Request, res: Response) {
        const games = await pool.then((r: any) => r.query('SELECT * FROM games'));
        res.json(games);
    }

    public async getOne(req: Request, res: Response) {
        const {id} = req.params;
        const games = await pool.then((r: any) => r.query('SELECT * FROM games WHERE id = ?', [id]));
//const games = pool.query('SELECT * FROM games');
        if (games.length > 0) {
            return res.json(games[0]);
        }
        res.status(404).json({text: 'El juego no existe'});
    }

    public async create(req: Request, res: Response) {
        await pool.then((r: any) => r.query('INSERT INTO games set ?', [req.body]));
        res.json({message: 'Juego guardado'});
    }

    public async delete(req: Request, res: Response) {
        const {id} = req.params;
        await pool.then((r: any) => r.query('DELETE FROM games WHERE id = ?', [id]));
        res.json({message: 'El juego ha sido eliminado'});
    }

    public async update(req: Request, res: Response) {
        const {id} = req.params;
        await pool.then((r: any) => r.query('UPDATE games set ? WHERE id = ?', [req.body, id]));
        res.json({message: 'El juego fue actualizado'});
    }
}

const gamesController = new GamesController();
export default gamesController;
