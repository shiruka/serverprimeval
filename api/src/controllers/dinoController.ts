import {Request, Response} from 'express';
import pool from '../../database';

class DinoController {

    public async listStarters(req: Request, res: Response) {

        const starters = await pool.then((r: any) => r.query('SELECT id_dino FROM isstarter'))
        res.json(starters);

    }

    public async listMarkings(req: Request, res: Response) {

        const markings = await pool.then((r: any) => r.query('SELECT * FROM markings'))
        res.json(markings);

    }

    public async listDinos(req: Request, res: Response) {

        const dinos = await pool.then((r: any) => r.query('SELECT * FROM dino'))
        res.json(dinos);

    }

    public async listGendino(req: Request, res: Response) {

        const markings = await pool.then((r: any) => r.query('SELECT * FROM gendino'))
        res.json(markings);

    }

    public async getDino(req: Request, res: Response) {
        const {id} = req.params;
        const dinos = await pool.then((r: any) => r.query('SELECT * FROM dino WHERE id = ?', [id]));
        if (dinos.length > 0) {
            return res.json(dinos[0]);
        }
        res.status(404).json({text: 'The dino doesnt exists'});
    }

    public async getMarking(req: Request, res: Response) {
        const {att} = req.params;
        const marking = await pool.then((r: any) => r.query('SELECT * FROM markings WHERE attribute = ?', [att]));
        if (marking.length > 0) {
            return res.json(marking[0]);
        }
        res.status(404).json({text: 'The marking doesnt exists'});
    }

    public async getStarter(req: Request, res: Response) {
        const {id} = req.params;
        const marking = await pool.then((r: any) => r.query('SELECT * FROM isstarter WHERE id_dino = ?', [id]));
        if (marking.length > 0) {
            return res.json(marking[0]);
        }
        res.status(404).json({text: 'The starter doesnt exists'});
    }

    public async insertMarking(req: Request, res: Response) {
        await pool.then((r: any) => r.query('INSERT INTO markings set ?', [req.body]));
        res.json({message: 'Marking inserterd'});
    }

    public async insertDino(req: Request, res: Response) {
        await pool.then((r: any) => r.query('INSERT INTO dino set ?', [req.body]));
        res.json({message: 'Dino inserterd'});
    }

    public async insertStarter(req: Request, res: Response) {
        await pool.then((r: any) => r.query('INSERT INTO isstarter set ?', [req.body]));
        res.json({message: 'Starter registered'});
    }

    public async deleteDino(req: Request, res: Response) {
        const {id} = req.params;
        await pool.then((r: any) => r.query('DELETE FROM dino WHERE id = ?', [id]));
        res.json({message: 'The dino has been removed'});
    }

    public async updateDino(req: Request, res: Response) {
        const {id} = req.params;
        await pool.then((r: any) => r.query('UPDATE dino set ? WHERE id = ?', [req.body, id]));
        res.json({message: 'The dino has been updated'});
    }

    public async updateMarking(req: Request, res: Response) {
        const {att} = req.params;
        await pool.then((r: any) => r.query('UPDATE markings set ? WHERE attribute = ?', [req.body, att]));
        res.json({message: 'The marking has been updated'});
    }

    /*

        public async create(req: Request, res: Response) {
            await pool.then((r: any) => r.query('INSERT INTO games set ?', [req.body]));
            res.json({message: 'Juego guardado'});
        }

        public async delete(req: Request, res: Response) {
            const {id} = req.params;
            await pool.then((r: any) => r.query('DELETE FROM games WHERE id = ?', [id]));
            res.json({message: 'El juego ha sido eliminado'});
        }

        public async update(req: Request, res: Response) {
            const {id} = req.params;
            await pool.then((r: any) => r.query('UPDATE games set ? WHERE id = ?', [req.body, id]));
            res.json({message: 'El juego fue actualizado'});
        }*/
}

const dinoController = new DinoController();
export default dinoController;
