import {Router} from 'express';
import dinoController from '../controllers/dinoController';

class DinoRoutes {
    public router: Router = Router();

    constructor() {
        this.config();
    }

    config(): void {
        this.router.get('/', dinoController.listDinos); //index

        this.router.get('/liststarters', dinoController.listStarters);//gets starter list
        this.router.get('/listmarkings', dinoController.listMarkings);//gets marking list
        this.router.get('/listdinos', dinoController.listDinos);//get dino list
        this.router.get('/listgendino', dinoController.listGendino);//gets genomes of dinos

        this.router.get('/getmarking/:att', dinoController.getMarking);//get info one marking
        this.router.get('/getdino/:id', dinoController.getDino);//get info one dino
        this.router.get('/getstarter/:id', dinoController.getStarter);//get if starter exists

        this.router.post('/insertmarking', dinoController.insertMarking);//inserts marking
        this.router.post('/insertdino', dinoController.insertDino);//inserts dino
        this.router.post('/insertstarter', dinoController.insertStarter);//registers a starter

        this.router.delete('/deletedino/:id', dinoController.deleteDino);//deletes a dino

        this.router.put('/updatedino/:id', dinoController.updateDino);//updates a dino (gendino not included)
        this.router.put('/updatemarking/:att', dinoController.updateMarking);//updates a marking

        /*this.router.get('/', dinoController.list);
        this.router.get('/:id', dinoController.getOne);
        this.router.post('/', dinoController.create);
        this.router.delete('/:id', dinoController.delete);
        this.router.put('/:id', dinoController.update);*/
    }
}

const dinoRoutes = new DinoRoutes();
export default dinoRoutes.router;
